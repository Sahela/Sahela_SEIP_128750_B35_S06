<?php
echo "<h3><u>Prime numbers between 1 -100:</u></h3><br>";
for($a = 2; $a <= 100; $a++)
{
    $isPrime = 0;
    for($b = 2; $b <= $a/2; $b++)
    {
        if($a % $b == 0)
        {
            $isPrime = 1;
            break;
        }

    }

    if($isPrime==0)
        echo $a."<br>";

}